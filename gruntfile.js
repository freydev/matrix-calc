module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        stylus: {
            compile: {
                files: {
                    'static/css/style.css': 'static/stylus/style.styl'
                }
            }
        },
        watch: {
            scripts: {
                files: 'static/stylus/*.styl',
                tasks: ['stylus'],
                options: {
                    debounceDelay: 250
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['stylus', 'watch']);
};