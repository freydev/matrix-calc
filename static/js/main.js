// ie8
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) {
                return i;
            }
        }
        return -1;
    }
}

// emulate :checked selector
ko.bindingHandlers.checkedCrutch = {
    init: function (el, v) {
        var value = v();
        if (value() == $(el).val()) $(el).addClass('checked')
    },

    update: function (el, v) {
        var value = v();
        $(el)[value() == $(el).val() ? 'addClass' : 'removeClass']('checked');

        // ie8 hack for repaint
        $(el).next().addClass('z').removeClass('z')
    }
};

ko.bindingHandlers.numeric = {
    init: function (el) {
        var allow_keys = [
            46, 8, 9, 27, 13, 110, // bksp, del, tab, esc, enter, point
            35, 36, 37, 39 // home, end, left, right
        ];

        $(el).on("keydown", function (event) {
            if (allow_keys.indexOf(event.keyCode) > -1) return
            else {
                // shift, !numeric, !numpad
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    }
};

var Matrix = function (row, col) {
    var self = this;
    this.matrix = ko.observableArray();
    this.colLength = ko.computed(function () {
        return self.matrix().length ? self.matrix()[0].row().length : 0
    });
    this.rowLength = ko.computed(function () {
        return self.matrix().length
    });
    for (var i = 0; i < row; i++)
        this.addRow(col, 1)
};

Matrix.prototype.truncate = function () {
    ko.utils.arrayMap(this.matrix(), function (row_obj) {
        ko.utils.arrayMap(row_obj.row(), function (row) {
            row.value(null)
        })
    })
};

Matrix.prototype.cell = function (row, col) {
    if (this.rowLength() > row && this.colLength() > col) {
        var row_obj = this.matrix()[row].row;
        return row_obj()[col]
    } else return 0
};

Matrix.prototype.removeCol = function () {
    ko.utils.arrayMap(this.matrix(), function (row_obj) {
        row_obj.row.pop()
    })
};

Matrix.prototype.removeRow = function () {
    this.matrix().pop();
    this.matrix.valueHasMutated();
};

Matrix.prototype.addCol = function () {
    ko.utils.arrayMap(this.matrix(), function (row_obj) {
        row_obj.row.push({value: ko.observable()})
    })
};

Matrix.prototype.addRow = function (cols_num) {
    if (typeof cols_num === 'undefined')
        cols_num = this.matrix()[0].row().length;

    var row = ko.observableArray();
    for (var i = 0; i < cols_num; i++)
        row.push({value: ko.observable()})
    this.matrix.push({row: row});
};

var viewModel = function () {
    var self = this,
        startRowA = 3,
        startColA = 2,
        startRowB = 2,
        startColB = 3;

    this.swapMatrix = function () {
        var temp = self.matrixB.matrix();
        self.matrixB.matrix(self.matrixA.matrix());
        self.matrixA.matrix(temp);
        self.resultMatrix.truncate()
    };

    this.mulMatrix = function () {
        var
            resultRows = self.resultMatrix.rowLength(),
            resultCols = self.resultMatrix.colLength();

        for (var i = 0; i < resultRows; i++) { // строки
            for (var j = 0; j < resultCols; j++) { // колонки
                var curCell = self.resultMatrix.cell(i, j);
                    curCell.value(0);
                for (var k = 0; k < self.matrixA.colLength(); k++) {
                    var result = curCell.value() + self.matrixA.cell(i, k).value() * self.matrixB.cell(k, j).value();
                    curCell.value(result);
                }
            }
        }
    };

    this.clear = function () {
        self.matrixA.truncate();
        self.matrixB.truncate()
    };

    this.matrixA = new Matrix(startRowA, startColA);
    this.matrixB = new Matrix(startRowB, startColB);
    this.resultMatrix = new Matrix(startRowA, startColB);

    this.matrixA.rowLength.subscribe(function (n) {
        if (n > self.resultMatrix.rowLength()) self.resultMatrix.addRow();
        else self.resultMatrix.removeRow()
    });

    this.matrixB.colLength.subscribe(function (n) {
        if (n > self.resultMatrix.colLength()) self.resultMatrix.addCol();
        else self.resultMatrix.removeCol()
    });

    this.activeMatrix = ko.observable('a');
    this.activeMatrix.subscribe(function (n) {
        self.activeMatrixLink(self[n == 'a' ? 'matrixA' : 'matrixB']);
    });

    this.activeMatrixLink = ko.observable(self.matrixA)
};

ko.applyBindings(viewModel);